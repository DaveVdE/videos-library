
define(['knockout', 'models/app', 'text!views/video.html'], function (ko, app, template) {
    function VideoModel(params) {
        var self = this;
        this.data = params;

        this.error = ko.observable(false);
        this.status = ko.observable();

        this.id = ko.observable();
        this.title = ko.observable();
        this.syncing = ko.observable(false);
        this.status = ko.observable();
        this.labels = ko.observableArray();
        this.newLabel = ko.observable();
        this.thumbnailUrl = ko.observable();
        this.videoUrl = ko.observable();

        this.set(params);

        this.url = ko.computed(function () {
            return '/api/video/' + self.id();
        });
        this.loading = ko.computed(function () {
            return !this.error() &&
                this.status() !== 'processing' && (
                    this.syncing() ||
                    this.status() === 'uploading'
                );
        }, this);

        this.title.subscribe(function () {
            this.save();
        }, this);

        this.newLabel.subscribe(function (newLabel) {
            if (newLabel) {
                this.labels.push(newLabel);
                this.newLabel(null);
                this.save();
            }
        }, this);

        this.checkStatus();

        this.removeLabel = function (label) {
        self.labels.remove(label);
        self.save();
    };

    }

    VideoModel.prototype.checkStatus = function () {
        var self = this;

        if (this.status() === 'processing') {
            setTimeout(function () {
                self.sync('GET', 'status').then(function () {
                    self.checkStatus();
                });
            }, 2000);
        }
    };

    VideoModel.prototype.openVideoPlayer = function () {
        app.openVideoPlayer(this);
    };

    VideoModel.prototype.set = function (params) {
        this.title(params.title);
        this.status(params.status);
        this.id(params.id);
        this.labels(params.labels);
        this.thumbnailUrl(params.thumbnail_url);
        this.videoUrl(params.video_url);
    };

    VideoModel.prototype.save = function () {
        if(this.id()) {
            this.sync('PATCH', {
                title: this.title(),
                labels: this.labels()
            });
        }
    };

    VideoModel.prototype.remove = function () {
        var self = this;

        if(this.id()) {
            this.sync('DELETE').then(function () {
                app.videos.remove(self.data);
            });
        } else {
            app.videos.remove(self.data);
        }
    };

    VideoModel.prototype.sync = function (op, data) {
        var self = this, action = '';
        this.syncing(true);
        this.error(false);

        if (typeof data === 'string') {
            action = '/' + data;
            data = null;
        }

        return $.ajax(this.url() + action, {
            type: op,
            data: JSON.stringify(data || {}),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
        }).then(function (result) {
            if (result) {
                self.set(result);
            }
            self.syncing(false);
        }, function (jqxhr) {
            self.syncing(false);
            self.error(jqxhr.statusText || 'Unexpected Error');
        });
    };

    ko.components.register('video-thumbnail', {
        viewModel: {
            createViewModel: function(params) {
                var model = new VideoModel(params);
                params.model = model;
                return model;
            }
        },
        template: template
    });

    return VideoModel;
});