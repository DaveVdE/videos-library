
var fs = require('fs'),
    AWS = require('aws-sdk'),
    funkify = require('funkify'),
    thunkify = require('thunkify'),
    mongoose = require('mongoose');


var read = thunkify(fs.readFile);

var VideoSchema = new mongoose.Schema({
    url: String,
    title: String,
    name: String,
    type: String,
    labels: [String],
    jobs: [String],
    status: String
}, {
    toJSON: {
        virtuals: true,
        transform: function (doc, ret, options) {
            delete ret._id;
        }
    }
});

VideoSchema.virtual('video_url').get(function () {
    if(this.status == 'success') {
        return {
            mp4: 'http://s3-eu-west-1.amazonaws.com/jsan-videos/video-' + this.id + '.mp4',
            webm: 'http://s3-eu-west-1.amazonaws.com/jsan-videos/video-' + this.id + '.webm'
        };
    }
});
VideoSchema.virtual('thumbnail_url').get(function () {
    if(this.status == 'success') {
        return 'http://s3-eu-west-1.amazonaws.com/jsan-videos/thumb-' + this.id + '-00001.png';
    }
});

var Video = mongoose.model('Video', VideoSchema);

/* AWS services */

var s3 = new AWS.S3({
    accessKeyId: process.env.AS3_ACCESS_KEY,
    secretAccessKey: process.env.AS3_SECRET_ACCESS_KEY,
    region: "eu-west-1",
    // endpoint: 's3-eu-west-1.amazonaws.com',
    params: {
        Bucket: 'jsan-videos',
    }
});
var elastictranscoder = new AWS.ElasticTranscoder({
    accessKeyId: process.env.AS3_ACCESS_KEY,
    secretAccessKey: process.env.AS3_SECRET_ACCESS_KEY,
    region: 'us-west-2'
    // endpoint: 'elastictranscoder.us-west-2.amazonaws.com',
});

s3 = funkify(s3);
elastictranscoder = funkify(elastictranscoder);

Video.prototype.s3upload = function* (file) {
    var self = this;
    var content = yield read(file.path);

    yield s3.putObject({
        Key: self.id,
        ACL: 'public-read',
        ContentType: file.type,
        Body: content,
    });
};
Video.prototype.s3transform = function* () {
    var job, self = this;
    this.jobs = [];

    job = yield elastictranscoder.createJob({
        PipelineId: '1418071188970-4tbvhm',
        Input: {
            Key: this.id
        },
        Output: {
            Key: 'video-' + this.id + '.mp4',
            PresetId: '1418573554391-j94ljl',
            ThumbnailPattern: 'thumb-' + this.id + '-{count}'
        }
    });
    this.jobs.push(job.Job.Id);
    job = yield elastictranscoder.createJob({
        PipelineId: '1418071188970-4tbvhm',
        Input: {
            Key: this.id
        },
        Output: {
            Key: 'video-' + this.id + '.webm',
            PresetId: '1418573373492-tv60ae'
        }
    });
    this.jobs.push(job.Job.Id);
    this.status = 'processing';
    this.save();
};

Video.prototype.s3status = function* () {
    var len = this.jobs.length;
    // remove ids from jobs, so start from the end
    for (var i = len - 1; i >= 0; i--) {
        var result = yield elastictranscoder.readJob({
            Id: this.jobs[i]
        });
        if (result.Job.Output.Status === 'Complete') {
            this.jobs.splice(i, 1);
        } else if (result.Job.Output.Status === 'Error') {
            this.status = 'error';
            break;
        }
    }
    if (!this.jobs.length) {
        this.status = 'success';
    }
    this.save();
};

module.exports = Video;
