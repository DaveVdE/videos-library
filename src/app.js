var koa = require('koa'),
    serve = require('koa-static'),
    logger = require('koa-logger');

var app = koa();

app.use(logger());
app.use(serve('static'));

require('./api').route(app);

module.exports = app;