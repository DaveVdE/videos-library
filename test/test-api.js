process.env.MONGOOSE_DISABLE_STABILITY_WARNING = true;

var mongoose = require('mongoose'),
    funkify = require('funkify'),
    expect = require('chai').expect,
    supertest = require('co-supertest'),
    app = require('../src/app'),
    Video = require('../src/models/video');

var request = supertest.agent(app.listen());
mongoose = funkify(mongoose);

describe('api', function () {
    before(function* () {
        yield mongoose.connect('mongodb://localhost/test-video-library');
    });

    describe('video', function () {

        beforeEach(function* () {
            this.vid1 = yield new Video({
                id: '124',
                status: 'success'
            }).save();
            this.vid2 = yield new Video({
                id: '456',
                status: 'success'
            }).save();
        });

        it('should get object in db', function* () {
            var response = yield request
                .get('/api/video/' + this.vid1.id)
                .expect(200)
                .expect('Content-Type', /json/)
                .end();

            expect(response.body).to.have.property('id', this.vid1.id);
            expect(response.body).to.have.property('labels');
            expect(response.body).to.have.property('thumbnail_url');
        });

        it('should list object in db', function* () {
            var response = yield request
                .get('/api/videos')
                .expect(200)
                .expect('Content-Type', /json/)
                .end();

            expect(response.body).to.have.length(2);
            expect(response.body[0]).to.have.property('id', this.vid1.id);
        });

        it('should delete object in db', function* () {
            yield request
                .delete('/api/video/' + this.vid1.id)
                .expect(200)
                .expect('Content-Type', /json/)
                .end();

            yield request
                .get('/api/video/' + this.vid1.id)
                .expect(404)
                .expect('Content-Type', /json/)
                .end();
        });

        afterEach(function* () {
            yield Video.find({}).remove().exec();
        });
    });

});