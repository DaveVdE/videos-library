var gulp = require('gulp');
var path = require('path');
var $ = require('gulp-load-plugins')();

gulp.task('less', function () {
    return gulp.src('./less/main.less')
        .pipe($.less({
            paths: [
                path.join(__dirname, 'static', 'components')
            ]
        }))
        .pipe(gulp.dest('./static/css'));
});

gulp.task('watch-less', ['less'], function () {
    gulp.watch('less/**/*.less', ['less']);
});

gulp.task('jshint', function () {
    return gulp.src('static/js/**/*.js')
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish'))
        .pipe($.jshint.reporter('fail'));
});

// gulp.task('serve', function () {
//     $.connect.server({
//         root: '.',
//         port: 8000,
//     });
// });

gulp.task('default', ['jshint']);
gulp.task('dist', ['less']);
// gulp.task('watch', ['serve', 'watch-less']);
